package fr.dyl.consumer.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaConsumerListener {

    @KafkaListener(
            topics = "myTopic",
            groupId = "groupId")
    public void listener(String data) {
        System.out.println("➾ Listener received " + data + " 🥳");
    }
}
