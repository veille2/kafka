# kafka

Kafka is a distributed system consisting of servers and clients that communicate via a high-performance TCP network protocol.

## Server
forms the storage layer "broker" 
or
run Kafka Connect to continuously import and export data as event streams to integrate kafka with an existing system such as databases

## Clients
They allow you to write distributed applications and microservices that read, write, and process streams of events in parallel,

## Event 
== record or message
It records the fact that "something happened" in the world or in your business.
Conceptually, an event has a key, value, timestamp, and optional metadata

## Producers 
Those client applications that publish (write) events to Kafka,

## Consumers
Those that subscribe to (read and process) these events.

# Read messages from the topic

Launch the kafka-console-consumer.
The --from-beginning argument means that messages will be read from the start of the topic.

```docker exec --interactive --tty broker \
kafka-console-consumer --bootstrap-server broker:9092 \
--topic myTopic \
--from-beginning


# Ref: 
https://kafka.apache.org/intro#intro_nutshell 
https://developer.confluent.io/quickstart/kafka-docker/
