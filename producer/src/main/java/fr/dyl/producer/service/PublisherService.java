package fr.dyl.producer.service;

import fr.dyl.producer.dto.MessageRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PublisherService {

    private static final String TOPIC_NAME = "myTopic";

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void publish(String message) {
        kafkaTemplate.send(TOPIC_NAME, message);
    }
}
