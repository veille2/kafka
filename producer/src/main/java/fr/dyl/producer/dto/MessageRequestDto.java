package fr.dyl.producer.dto;

public record MessageRequestDto(String message) {
}
