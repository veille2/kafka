package fr.dyl.producer.endpoint;

import fr.dyl.producer.dto.MessageRequestDto;
import fr.dyl.producer.service.PublisherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/messges")
@RequiredArgsConstructor
public class MessageController {

    private final PublisherService publisherService;

    @PostMapping
    public void publish(@RequestBody MessageRequestDto messageRequest) {
        publisherService.publish(messageRequest.message());
    }
}
